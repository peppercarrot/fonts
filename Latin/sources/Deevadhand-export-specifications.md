# Fontforge notes

## Lavi glyph settings

### Bold:

* Element>Styles menu>Change weight.
* CJK, Embolden by 42em units + Fuzz = 0, Counters Squish, Cleanup Self Intersect 'on'.

### Italic:

* Element>Styles menu>Italic.
* (uncheck all styles), radio button 'Flat'
